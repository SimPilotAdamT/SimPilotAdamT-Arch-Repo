# My Arch Linux repository

This repo has come packages I use precompiled for x86_64 (no 32-bit Arch or ArchLinuxArm support). To use it, add the following to the bottom of your `pacman.conf` file:

```
[SimPilotAdamT-Arch-Repo]
Server = https://gitlab.com/SimPilotAdamT/$repo/-/raw/main/$arch/
```

Also make sure to add my key to your Pacman keyring:

```bash
pacman-key --recv-keys 86E2FA59650FFC0B3E093AE2060D5B38E279B5ED;
pacman-key --lsign-key 86E2FA59650FFC0B3E093AE2060D5B38E279B5ED;
```
